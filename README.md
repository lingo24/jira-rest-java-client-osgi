Lingo24 OSGI jira-rest-java-client
==================================

This OSGi bundle simply wraps [mvn:com.atlassian.jira/jira-rest-java-client/1.2-m01](https://bitbucket.org/atlassian/jira-rest-java-client/src/1cb69aca99f5c14bf72b8c25f364ba0d47b9d148/?at=jira-rest-java-client-1.2-m01) artifact and it's dependencies using the Apache Felix Maven Bundle Plugin (BND).

It can then be loaded as an OSGi bundle 

```xml
<bundle>
  mvn:com.lingo24.osgi/jira-rest-java-client/${lingo24-osgi.jira-rest-java-client.version}
<bundle>
```

## Maven commands

To build this project use

	mvn clean install

You may need to add new repositories to your maven settings:

```xml

<?xml version="1.0" encoding="UTF-8"?>
<settings xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd" xmlns="http://maven.apache.org/SETTINGS/1.1.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
[...]
  <profiles>
    <profile>
      [...]
      <repositories>
        [...]

        <!-- Third Party Repositories -->
        <repository>
          <id>fusesource.m2</id>
          <name>FuseSource Community Release Repository</name>
          <url>https://repo.fusesource.com/nexus/content/groups/public</url>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
          </releases>
        </repository>
        <repository>
          <id>fusesource.ea</id>
          <name>FuseSource Community Early Access Release Repository</name>
          <url>https://repo.fusesource.com/nexus/content/groups/ea</url>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
          </releases>
        </repository>
        <repository>
          <id>atlassian-public</id>
          <url>https://maven.atlassian.com/content/groups/public</url>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
          </snapshots>
          <releases>
            <enabled>true</enabled>
            <checksumPolicy>warn</checksumPolicy>
          </releases>
        </repository>

        [...]
      </repositories>
      <pluginRepositories>
        [...]

        <!-- Third Party Repositories -->
        <pluginRepository>
          <id>fusesource.m2</id>
          <name>FuseSource Community Release Repository</name>
          <url>https://repo.fusesource.com/nexus/content/groups/public</url>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
          </releases>
        </pluginRepository>
        <pluginRepository>
          <id>fusesource.ea</id>
          <name>FuseSource Community Early Access Release Repository</name>
          <url>https://repo.fusesource.com/nexus/content/groups/ea</url>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <releases>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
          </releases>
        </pluginRepository>
        <pluginRepository>
          <id>atlassian-public</id>
          <url>https://maven.atlassian.com/content/groups/public</url>
          <snapshots>
            <enabled>true</enabled>
            <updatePolicy>never</updatePolicy>
            <checksumPolicy>warn</checksumPolicy>
          </snapshots>
          <releases>
            <enabled>true</enabled>
            <checksumPolicy>warn</checksumPolicy>
          </releases>
        </pluginRepository>

        [...]
      </pluginRepositories>
      [...]
    </profile>
  </profiles>
</settings>
```

## Online Documentation
_For more help see the_

* [Apache Felix Maven Bundle Plugin (BND)](http://felix.apache.org/documentation/subprojects/apache-felix-maven-bundle-plugin-bnd.html)

* [Fabric8 Maven Plugin](http://fabric8.io/gitbook/mavenPlugin.html) & [Fabric configuration properties](https://access.redhat.com/documentation/en-US/Red_Hat_JBoss_Fuse/6.1/html/Fabric_Guide/F8Plugin-Props.html)